Spree::Product.class_eval do
  scope :related_products, -> (product) { in_taxons(product.taxons).where.not(id: product.id).distinct } # rubocop:disable Style/LineLength
  scope :includes_img_and_price, -> { includes(master: [:default_price, :images]) }
end
