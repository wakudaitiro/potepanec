class Potepan::Api::V1::SuggestsController < Potepan::Api::ApiController
  include ActionController::HttpAuthentication::Token::ControllerMethods

  def suggest
    return render json: { status: "error", code: 400, content: { message: "keywordが存在しません" } }, status: 400 if params[:keyword].nil? # rubocop:disable Style/LineLength

    suggests = if params[:max_num].to_i.positive?
                 Potepan::Suggest.search_by_keyword(params[:keyword]).limit(params[:max_num])
               else
                 Potepan::Suggest.search_by_keyword(params[:keyword])
               end.pluck(:keyword)
    render json: suggests
  end
end
