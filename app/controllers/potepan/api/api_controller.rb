class Potepan::Api::ApiController < ApplicationController
  before_action :authenticate

  rescue_from StandardError, with: :rescue500

  protected

  def rescue500(e)
    logger.info "Rendering 500 with exception: #{e.message}"
    render json: { status: "error", code: 500, content: { message: "内部サーバーエラー" } }, status: 500
  end

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, options|
      myapi_key = Rails.application.credentials.my_api[:my_suggest_key]
      ActiveSupport::SecurityUtils.secure_compare(token, myapi_key)
    end
  end

  def render_unauthorized
    render json: { status: "error", code: 401, content: { message: "権限がありません" } }, status: 401
  end
end
