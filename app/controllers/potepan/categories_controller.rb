class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxon.roots
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes_img_and_price
  end
end
