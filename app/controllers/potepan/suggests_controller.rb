require 'httpclient'

class Potepan::SuggestsController < ApplicationController
  def suggest
    client = HTTPClient.new
    url = Rails.application.credentials.api[:suggest_url]
    headers = { "Authorization" => "Bearer #{Rails.application.credentials.api[:suggest_key]}" }
    query = {
      "keyword" => params[:keyword],
      "max_num" => SUGGEST_MAX_COUNT,
    }
    response = client.get(url, query, headers)
    if response.status == 200
      render json: JSON.parse(response.body)
    else
      render json: [], status: response.status
    end
  end
end
