class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAXIMUM = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).limit(RELATED_PRODUCTS_MAXIMUM).includes_img_and_price # rubocop:disable Style/LineLength
  end
end
