require 'rails_helper'
require 'webmock/rspec'

describe Potepan::SuggestsController, type: :request do
  describe 'Ajax通信によるデータ取得' do
    context 'keywordを入力した場合' do
      before do
        WebMock.enable!
        stub_request(
          :get, "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests"
        ).with(
          headers: { "Authorization" => "Bearer api123" },
          query: { 'keyword' => 'ru', 'max_num' => 5 }
        ).to_return(
          body: ['ruby', 'ruby for women', 'ruby for men', 'RUBY ON RAILS', 'RUBY ON RAILS bag'].to_json, # rubocop:disable Style/LineLength
          status: 200,
          headers: { 'Content-Type' => 'application/json' }
        )
      end

      it 'リクエストに対し200レスポンスを返すこと' do
        get '/potepan/suggest', params: { keyword: 'ru', max_num: '5' }
        expect(response.status).to eq(200)
      end

      it '文字列ruを含んだ候補を5個返すこと' do
        get '/potepan/suggest', params: { keyword: 'ru', max_num: '5' }
        expect(JSON.parse(response.body)).to eq ['ruby', 'ruby for women', 'ruby for men', 'RUBY ON RAILS', 'RUBY ON RAILS bag'] # rubocop:disable Style/LineLength
      end
    end

    context 'keywordが空の場合' do
      before do
        WebMock.enable!
        stub_request(
          :get, "https://presite-potepanec-task5.herokuapp.com/potepan/api/suggests"
        ).with(
          headers: { "Authorization" => "Bearer api123" },
          query: { "keyword" => nil, "max_num" => 5 }
        ).to_return(
          status: 500,
          headers: { 'Content-Type' => 'application/json' }
        )
      end

      it 'リクエストに対し500レスポンスを返すこと' do
        get '/potepan/suggest'
        expect(response.status).to eq(500)
      end
    end
  end
end
