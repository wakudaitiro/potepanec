require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "products/showページへアクセス" do
    before { get potepan_product_path(product.id) }

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status(200)
    end

    it "showテンプレートが表示されること" do
      expect(response).to render_template :show
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "商品の値段が表示されること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品説明が表示されること" do
      expect(response.body).to include product.description
    end

    it "関連商品が５個以上の場合４件まで表示されること" do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end
