require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "categories/showページへアクセス" do
    before { get potepan_category_path(taxon.id) }

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status(200)
    end

    it "showテンプレートが表示されること" do
      expect(response).to render_template :show
    end

    it "taxonomyが取得できること" do
      expect(response.body).to include taxonomy.name
    end

    it "taxonが取得できること" do
      expect(response.body).to include taxon.name
    end

    it "商品名が表示されること" do
      expect(response.body).to include product.name
    end

    it "商品の値段が表示されること" do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
