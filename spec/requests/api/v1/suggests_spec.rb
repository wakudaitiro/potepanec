require 'rails_helper'

describe Potepan::Api::V1::SuggestsController, type: :request do
  describe 'GET /api/v1/suggest' do
    let!(:keyword1) { create(:suggest, keyword: "rugby") }
    let!(:keyword2) { create(:suggest, keyword: "ruby") }
    let!(:keyword3) { create(:suggest, keyword: "ruby bag") }
    let!(:keyword4) { create(:suggest, keyword: "ruby cup") }
    let(:params) { { keyword: "r", max_num: 3 } }
    let(:headers) { { 'Authorization' => "Bearer #{Rails.application.credentials.my_api[:my_suggest_key]}" } } # rubocop:disable Style/LineLength

    before do
      get potepan_api_v1_suggest_path, params: params, headers: headers
    end

    context 'max_numに数値を指定した場合' do
      let(:params) { { keyword: "ruby", max_num: 2 } }

      it 'リクエスト時に200レスポンスが返ってくる' do
        expect(response.status).to eq(200)
      end

      it '指定した数の検索候補を取得' do
        json = JSON.parse(response.body)
        expect(json).to match_array [keyword2[:keyword], keyword3[:keyword]]
      end
    end

    context 'max_numを指定しない場合' do
      let(:params) { { keyword: "ruby" } }

      it 'keywordの文字列を含む全ての検索候補を取得' do
        json = JSON.parse(response.body)
        expect(json).to match_array [keyword2[:keyword], keyword3[:keyword], keyword4[:keyword]]
      end
    end

    context 'max_numに数値以外を指定した場合' do
      let(:params) { { keyword: 'ruby', max_num: "a" } }

      it 'keywordの文字列を含む全ての検索候補を取得' do
        json = JSON.parse(response.body)
        expect(json).to match_array [keyword2[:keyword], keyword3[:keyword], keyword4[:keyword]]
      end
    end

    context 'max_numに負の数を指定した場合' do
      let(:params) { { keyword: 'ruby', max_num: -1 } }

      it 'keywordの文字列を含む全ての検索候補を取得' do
        json = JSON.parse(response.body)
        expect(json).to match_array [keyword2[:keyword], keyword3[:keyword], keyword4[:keyword]]
      end
    end

    context 'クエリにkeywordが存在しない場合' do
      let(:params) { { max_num: 3 } }

      it '400レスポンスが返ってくる' do
        expect(response.status).to eq(400)
      end
    end

    context '認証キーを間違えた場合' do
      let(:headers) { { 'Authorization' => 'Bearer testkey' } }

      it '401レスポンスが返ってくる' do
        expect(response.status).to eq(401)
      end
    end
  end
end
