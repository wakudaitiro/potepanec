require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let!(:taxon1) { create(:taxon) }
    let!(:taxon2) { create(:taxon) }
    let!(:taxon3) { create(:taxon) }
    let!(:product1) { create(:product, taxons: [taxon1]) }
    let!(:product2) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product3) { create(:product, taxons: [taxon1, taxon2]) }
    let!(:product4) { create(:product, taxons: [taxon1, taxon3]) }
    let!(:product5) { create(:product, taxons: [taxon1, taxon3]) }
    let!(:product6) { create(:product, taxons: [taxon1, taxon3]) }
    let!(:product7) { create(:product, taxons: [taxon3]) }
    let!(:related_products) { Spree::Product.related_products(product1).includes_img_and_price }

    it "decoratorで定義したメソッドが使えることを確認" do
      # product1と1つでもtaxonが同じ商品をすべて取得する
      expect(related_products).to eq [product2, product3, product4, product5, product6]
      # product1とtaxonが１つも一致しない商品は含まれない
      expect(related_products).not_to include product7
    end
  end
end
