require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let!(:taxonomy_category) { create(:taxonomy, name: 'Category') }
  let!(:taxonomy_brand) { create(:taxonomy, name: 'Brand') }
  let!(:bag_taxon) { create(:taxon, name: 'Bags', taxonomy: taxonomy_category) }
  let!(:mug_taxon) { create(:taxon, name: 'Mugs', taxonomy: taxonomy_category) }
  let!(:rails_taxon) { create(:taxon, name: 'Rails', taxonomy: taxonomy_brand) }
  let!(:ruby_taxon) { create(:taxon, name: 'Ruby', taxonomy: taxonomy_brand) }
  let!(:rails_tote) { create(:product, name: 'Rails-Tote', price: '$15.99', taxons: [bag_taxon, rails_taxon]) } # rubocop:disable Style/LineLength
  let!(:rails_bag) { create(:product, name: 'Rails-Bag', taxons: [bag_taxon, rails_taxon]) }
  let!(:rails_mug) { create(:product, name: 'Rails-Mug', taxons: [mug_taxon, rails_taxon]) }
  let!(:ruby_bag) { create(:product, name: 'Ruby-Bag', taxons: [bag_taxon, ruby_taxon]) }
  let!(:ruby_mug) { create(:product, name: 'Ruby-Mug', taxons: [mug_taxon, ruby_taxon]) }
  let(:add_rails_bag) { create_list(:product, 2, name: 'Add_Rails_Bag', taxons: [bag_taxon]) }

  before do
    visit potepan_product_path(rails_tote.id)
  end

  scenario "一覧へ戻るをクリックするとその商品のカテゴリーページへアクセス" do
    expect(page).to have_content rails_tote.name
    expect(page).to have_content rails_tote.price
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(bag_taxon.id)
  end

  scenario '関連商品をクリックすると該当の商品詳細ページへ移動' do
    find('.productsContent').click_on ruby_bag.name
    expect(current_path).to eq potepan_product_path(ruby_bag.id)
  end

  scenario '関連した商品のみが重複なく表示、関連しない及び関連元商品は非表示' do
    within find('.productsContent') do
      # 関連する商品
      expect(page).to have_content rails_bag.name, count: 1
      expect(page).to have_content rails_mug.name, count: 1
      expect(page).to have_content ruby_bag.name, count: 1
      # 関連しない商品
      expect(page).not_to have_content ruby_mug.name
      # 関連元の商品
      expect(page).not_to have_content rails_tote.name
      expect(all('.productBox').count).to eq(3)
    end
  end

  scenario '関連した商品のみが５個以上の場合は４個まで表示' do
    within find('.productsContent') do
      # letで宣言したadd_rails_bag２つを作成するため以下のコードを追加
      add_rails_bag
      visit current_path
      expect(all('.productBox').count).to eq(4)
    end
  end
end
