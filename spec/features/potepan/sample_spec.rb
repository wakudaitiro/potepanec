require 'rails_helper'

RSpec.feature 'ECトップページ' do
  before do
    visit potepan_root_path
  end

  feature 'ヘッダー' do
    scenario '検索バー表示' do
      expect(page).to have_selector('.searchBox')
    end
  end
end
