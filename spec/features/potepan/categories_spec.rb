require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:other_taxon) { create(:taxon, name: "Mugs", parent_id: taxonomy.root.id, taxonomy: taxonomy) } # rubocop:disable Style/LineLength
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, name: 'Rails-Mug', price: '$13.99', taxons: [other_taxon]) } # rubocop:disable Style/LineLength

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "サイドバーの「商品の種類（数）」のリンクをクリックするとそのtaxonに関連した商品名、商品の価格が表示され、taxonに関連しない商品の情報は表示されない" do
    expect(page).to have_link "#{taxon.name} (#{taxon.products.count})"
    click_link "#{taxon.name} (#{taxon.products.count})"
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).not_to have_content other_product.name
    expect(page).not_to have_content other_product.display_price
  end

  scenario "サイドバーの「別の商品の種類（数）」のリンクをクリックするとその別のtaxonに関連した商品名、商品の価格が表示され、taxonに関連しない商品の情報は表示されない" do
    click_link "#{other_taxon.name} (#{other_taxon.products.count})"
    expect(page).to have_content other_product.name
    expect(page).to have_content other_product.display_price
    expect(page).not_to have_content product.name
    expect(page).not_to have_content product.display_price
  end

  scenario "商品名のリンクをクリックすると商品詳細ページが開く" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
